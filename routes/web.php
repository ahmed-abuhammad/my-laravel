<?php
use App\User;
use App\Post;
use App\Photo;
use App\Country;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Access the many-to-many relationship table
Route::get("/user/pivot", function() {
	$user = User::find(1);

	foreach ($user->roles as $role)
	{
		echo $role->pivot->created_at . "<br>";
	}
});

// has-many through relationship
Route::get("country/{id}/posts", function($id) {
	$country = Country::find($id);
	return $country->posts;

});

// Polymorhic
Route::get("/user/{id}/photos", function($id) {
	$user = User::find($id);
	return $user->photos;
});

Route::get("/post/{id}/photos", function($id) {
	$post = Post::find($id);
	return $post->photos;
});

Route::get("photo/{id}/user", function($id) {
	$photo = Photo::findOrFail($id);
	// This will return the object of App\User or App\Post
	// That the photo is polyphored to
	return $photo->imageable;
});