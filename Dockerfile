FROM php:7.3.0-fpm

RUN apt update && apt install -y libmcrypt-dev \
	mysql-client libmagickwand-dev --no-install-recommends

RUN docker-php-ext-install pdo_mysql mbstring tokenizer xml ctype json bcmath

RUN groupadd -r -g 1000 ahmed && useradd -r -u 1000 -g ahmed ahmed

USER ahmed