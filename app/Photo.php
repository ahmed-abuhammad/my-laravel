<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    // To be a polymorphic model
    // The imageable_type must be the spacename
    // Of the related models
    public function imageable()
    {
    	return $this->morphTo();
    }
}
