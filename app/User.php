<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany("App\Role")->withPivot(["created_at"]);

        // Or use this format if the pivot table is not role_user
        // To customize table name and column names
        return $this->belongsToMany("App\Role", "role_user", "user_id", "role_id");
    }

    public function photos()
    {
        return $this->morphMany("App\Photo", "imageable");
    }
}
