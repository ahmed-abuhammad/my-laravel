<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    public function posts()
    {
    	// Get all the posts of all the users of this country
    	// Country -> users using country_id -> posts using user_id
    	return $this->hasManyThrough("App\Post", "App\User");

    	// This format if we have different names for columns
    	// country_id is the middle table (users) foreign key to the countries table
    	// return $this->hasManyThrough("App\Post", "App\User", "country_id", "user_id");
    }
}
